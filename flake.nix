{
  description = "Brain train - Train your brain on... on... trains! Ha ha!";

  # TODO: add a cuda enabled shell and make default without cuda

  nixConfig = {
    extra-substituters = [
      "https://ai.cachix.org"
      "https://cuda-maintainers.cachix.org"
    ];
    extra-trusted-public-keys = [
      "ai.cachix.org-1:N9dzRK+alWwoKXQlnn0H6aUx0lU/mspIoz8hMvGvbbc="
      "cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E="
    ];
  };

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    poetry2nix.url = "github:nix-community/poetry2nix";
    poetry2nix.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, ... } @rawInputs:
    let
      systems = flake-utils.lib.defaultSystems;
    in
    builtins.foldl'
      (outputs: system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlay = [ poetry2nix.overlay ];
            config = {
              allowUnfree = true;
              cudaSupport = true;
            };
          };
          poetry2nix = (rawInputs.poetry2nix.lib.mkPoetry2Nix { inherit pkgs; });

          pypkgs-build-requirements = {
            bangla = [ "setuptools" ];
            bnunicodenormalizer = [ "setuptools" ];
            bnnumerizer = [ "setuptools" ];
            fsspec = [ "hatchling" ];
            hangul-romanize = [ "setuptools" ];
            gruut-lang-en = [ "setuptools" ];
            gruut-lang-fr = [ "setuptools" ];
            gruut-lang-de = [ "setuptools" ];
            gruut-lang-es = [ "setuptools" ];
            jamo = [ "setuptools" ];
            itsdangerous = [ "flit-core" ];
            lazy-loader = [ "setuptools" ];
            argbind = [ "setuptools" ];
            randomname = [ "setuptools" ];
          };

          postPatchWheel = postPatch: ''
            cd dist
            ls -la
            whl="$(basename ./*.whl)"
            unzip $whl
            rm $whl
          '' + postPatch + ''
            zip -r ../$whl ./*
            cd ..
            rm -rf dist
            mkdir dist
            mv $whl dist
          '';

          env = poetry2nix.mkPoetryEnv {
            projectDir = ./.;
            preferWheels = true;
            editablePackageSources = {
              brain-train = ./brain_train;
            };
            overrides = poetry2nix.defaultPoetryOverrides.extend
              (final: prev:
                (builtins.mapAttrs
                  (package: build-requirements:
                    (builtins.getAttr package prev).overridePythonAttrs (old: {
                      buildInputs = (old.buildInputs or [ ])
                      ++ (builtins.map
                        (pkg:
                          if builtins.isString pkg
                          then builtins.getAttr pkg prev
                          else pkg)
                        build-requirements);
                    }))
                  pypkgs-build-requirements) //
                {
                  soundfile = prev.soundfile.overridePythonAttrs (old: {
                    nativeBuildInputs = (old.buildInputs or [ ]) ++ [ pkgs.unzip pkgs.zip ];
                    postPatch = postPatchWheel old.postPatch;
                  });

                  torch = prev.torch.overridePythonAttrs (old: {
                    nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [
                      pkgs.autoPatchelfHook
                      pkgs.addDriverRunpath
                    ];
                    buildInputs = (old.buildInputs or [ ]) ++ (with pkgs; [
                      cudaPackages.cudnn
                      cudaPackages.cuda_nvrtc
                      cudaPackages.nccl
                      cudaPackages.cuda_cudart
                    ]);
                    extraRunpaths = [ "${prev.lib.getLib pkgs.cudaPackages.cuda_nvrtc}/lib" ];
                    postPhases = prev.lib.optionals pkgs.stdenv.hostPlatform.isUnix [ "postPatchelfPhase" ];
                    postPatchelfPhase = ''
                      while IFS= read -r -d $'\0' elf ; do
                        for extra in $extraRunpaths ; do
                          echo patchelf "$elf" --add-rpath "$extra" >&2
                          patchelf "$elf" --add-rpath "$extra"
                        done
                      done < <(
                        find "''${!outputLib}" "$out" -type f -iname '*.so' -print0
                      )
                    '';
                  });

                  parler-tts = prev.parler-tts.overridePythonAttrs (old: {
                    dontUseWheelUnpack = true;
                  });


                  torchaudio = prev.torchaudio.overridePythonAttrs (old: {
                    buildInputs = (old.buildInputs or [ ]) ++ (with pkgs; [
                      ffmpeg_4
                      ffmpeg_5
                      ffmpeg
                      sox
                    ]);
                  });

                  torchvision = prev.torchvision.overridePythonAttrs (old: {
                    nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [
                      pkgs.autoPatchelfHook
                    ];
                  });

                  llama-cpp-python = prev.llama-cpp-python.overridePythonAttrs (old: {
                    buildInputs = (old.buildInputs or [ ]) ++ (with pkgs; [
                      prev.scikit-build-core
                      vulkan-headers
                      vulkan-loader
                    ]);
                    env = {
                      CMAKE_ARGS = "-DLLAMA_VULKAN=on";
                    };
                  });

                  auto-gptq = prev.auto-gptq.overridePythonAttrs (old: {
                    buildInputs = (old.buildInputs or [ ]) ++ (with pkgs; [
                      libtorch-bin
                      cudaPackages.cuda_cudart
                    ]);
                  });

                  pyright = prev.pyright.overridePythonAttrs (old: {
                    postInstall = (old.postInstall or "") + ''
                      wrapProgram $out/bin/pyright \
                        --prefix PATH : ${pkgs.lib.makeBinPath [ pkgs.nodejs ]}
                      wrapProgram $out/bin/pyright-langserver \
                        --prefix PATH : ${pkgs.lib.makeBinPath [ pkgs.nodejs ]}
                    '';
                  });

                  sounddevice = prev.sounddevice.overridePythonAttrs (old: {
                    nativeBuildInputs = (old.buildInputs or [ ]) ++ [ pkgs.unzip pkgs.zip ];
                    postPatch = postPatchWheel ''
                      substituteInPlace sounddevice.py \
                        --replace "_find_library(_libname)" \
                        "'${pkgs.portaudio.out}/lib/libportaudio${pkgs.stdenv.hostPlatform.extensions.sharedLibrary}'"
                    '';
                  });

                  tts = prev.tts.overridePythonAttrs (old: {
                    nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [
                      pkgs.unzip
                      pkgs.zip
                      pkgs.autoPatchelfHook
                      pkgs.addDriverRunpath
                    ];
                    buildInputs = (old.buildInputs or [ ]) ++ (with pkgs; [
                      cudaPackages.cudnn
                      cudaPackages.cuda_nvrtc
                      cudaPackages.nccl
                      cudaPackages.cuda_cudart
                    ]);
                    postPatch = postPatchWheel ''
                      substituteInPlace TTS/tts/utils/text/phonemizers/espeak_wrapper.py \
                        --replace "_ESPEAK_LIB = _DEF_ESPEAK_LIB" \
                        "_ESPEAK_LIB = 'espeak'"
                      substituteInPlace TTS/tts/utils/text/phonemizers/espeak_wrapper.py \
                        --replace "_ESPEAK_VER = _DEF_ESPEAK_VER" \
                        "_ESPEAK_VER = '${pkgs.espeak.version}'"
                      substituteInPlace TTS/tts/utils/text/phonemizers/espeak_wrapper.py \
                        --replace "match.group(\"version\")" \
                        "'${pkgs.espeak.version}'"
                      substituteInPlace TTS/tts/utils/text/phonemizers/espeak_wrapper.py \
                        --replace "espeak_lib," \
                        "'${pkgs.espeak}/bin/espeak',"
                      substituteInPlace TTS/tts/utils/text/phonemizers/espeak_wrapper.py \
                        --replace "which(name)" \
                        "'${pkgs.espeak}/bin/espeak'"
                    '';
                  });
                });
          };

          mkEnvWrapper = name: pkgs.writeShellApplication {
            name = name;
            runtimeInputs = [ env ];
            text = ''
              export PYTHONPREFIX=${env}
              export PYTHONEXECUTABLE=${env}/bin/python

              # shellcheck disable=SC2125
              export PYTHONPATH=${env}/lib/**/site-packages

              ${name} "$@"
            '';
          };
        in
        outputs // {
          devShells.${system}.default = pkgs.mkShell {
            packages = with pkgs; [
              # Nix
              nil
              nixpkgs-fmt

              # Python
              poetry
              (mkEnvWrapper "pyright")
              (mkEnvWrapper "pyright-langserver")
              env

              # Misc
              nodePackages.prettier
              nodePackages.yaml-language-server
              marksman
              taplo

              # Tools
              nushell
              just
            ];
          };
        })
      { }
      systems;
}
