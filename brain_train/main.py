if __name__ == "__main__":
  from dotenv import load_dotenv
  import typer
  from async_wrap import async_wrap  # type: ignore[no-untyped-def] # copied signature
  # import os

  load_dotenv()
  # huggingface_token = os.getenv("BRAIN_TRAIN_HUGGINGFACE_HUB_ACCESS_TOKEN")
  # if huggingface_token is not None:
  #   from huggingface_hub import login
  #   login(token=os.environ["BRAIN_TRAIN_HUGGINGFACE_HUB_ACCESS_TOKEN"])

  app = typer.Typer()

  @app.command("ttt")
  def main_ttt(
    model: str,
    prompt: list[str],
  ):
    from ttt import ttt

    async_wrap(
      ttt,
      model,
      " ".join(prompt),
    )

  @app.command("tti")
  def main_tti(
    model: str,
    prompt: list[str],
  ):
    from tti import tti

    async_wrap(
      tti,
      model,
      " ".join(prompt),
    )

  @app.command("tts")
  def main_tts(
    model: str,
    description: str,
    prompt: list[str],
  ):
    from tts import tts

    async_wrap(
      tts,
      model,
      description,
      " ".join(prompt),
    )

  app()
