from diffusers.pipelines.stable_diffusion.pipeline_stable_diffusion import StableDiffusionPipeline


async def tti(model: str, prompt: str):
  pipeline = StableDiffusionPipeline.from_single_file(model).to('cuda')
  image = pipeline(prompt).images[0]
  image.show()
