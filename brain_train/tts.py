import sounddevice as sd
from parler_tts import ParlerTTSForConditionalGeneration
from transformers import AutoTokenizer
import soundfile as sf
import torch


async def tts(model: str, description: str, prompt: str):
  loaded_model = ParlerTTSForConditionalGeneration.from_pretrained(model).to(
    'cuda',
    dtype=torch.float16,
  )
  tokenizer = AutoTokenizer.from_pretrained(model)

  input_ids = tokenizer(description, return_tensors="pt").input_ids.to('cuda')
  prompt_input_ids = tokenizer(prompt, return_tensors="pt").input_ids.to('cuda')

  generation = loaded_model.generate(
    input_ids=input_ids,
    prompt_input_ids=prompt_input_ids,
  ).to(torch.float32)
  audio_arr = generation.cpu().numpy().squeeze()
  sd.play(audio_arr, samplerate=loaded_model.config.sampling_rate)
  sd.wait()
