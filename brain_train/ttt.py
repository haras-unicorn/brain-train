from langchain_community.llms import LlamaCpp
from langchain_core.callbacks import CallbackManager, StreamingStdOutCallbackHandler
from langchain_core.prompts import PromptTemplate


async def ttt(model: str, prompt: str):
  print(model)
  print(prompt)

  template = """Question: {prompt}

  Answer: Let's work this out in a step by step way to be sure we have the right answer."""
  prompt_template = PromptTemplate.from_template(template)

  callback_manager = CallbackManager([StreamingStdOutCallbackHandler()])

  n_gpu_layers = 100
  n_batch = 512

  llm = LlamaCpp(
    model_path=model,
    n_gpu_layers=n_gpu_layers,
    n_batch=n_batch,
    callback_manager=callback_manager,
    verbose=True,  # Verbose is required to pass to the callback manager
  )
  llm_chain = prompt_template | llm

  llm_chain.invoke({"prompt": prompt})
