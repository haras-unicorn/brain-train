from typing import Awaitable, Callable, Any
from signal import SIGINT, SIGTERM
import asyncio


def async_wrap(to_wrap: Callable[..., Awaitable[None]], *args: Any,
               **kwargs: Any):

  async def wrapper():
    try:
      await to_wrap(*args, **kwargs)
    except asyncio.CancelledError:
      pass

  loop = asyncio.get_event_loop()
  main_task = asyncio.ensure_future(wrapper())
  for signal in [SIGINT, SIGTERM]:
    loop.add_signal_handler(signal, main_task.cancel)
  try:
    loop.run_until_complete(main_task)
  finally:
    loop.close()
