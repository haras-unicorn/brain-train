set windows-shell := ["nu.exe", "-c"]
set shell := ["nu", "-c"]

root_path := justfile_directory()

default: prepare

prepare:
  cd "{{root_path}}"; poetry install --no-root

ci:
  cd "{{root_path}}"; poetry install --no-root

format:
  yapf --recursive --in-place --parallel "{{root_path}}"
  prettier --write "{{root_path}}"

lint:
  ruff check "{{root_path}}"

run *args:
  cd "{{root_path}}"; python ./brain_train/main.py {{args}}
